package com.zealcomm.ivcs.agent;

import com.zealcomm.base.entity.CustomMessage;
import com.zealcomm.ivcs.agent.base.entity.FormData;
import com.zealcomm.ivcs.agent.base.entity.FormListData;
import com.zealcomm.ivcs.agent.base.entity.UserInfo;


public interface IvcsAgentEvent {
    /**
     * when a new video stream joins or leave the room ，this event is triggered
     * @param videoLabel stream type
     * @param status true : joins,false : leave
     * */
    void onVideoLabel(String videoLabel,boolean status);
    /**
     * when receiving a resource link, this event is triggered
     * @param url resource url
     * @param type resource type(audio ,video,link,file,etc)
     * */
    void onResource(String url, String type,String name);
    /**
     * When a service is stopped, this event is triggered
     * @param reason reason for drop(user login in other device or service stopped,etc)
     * */
    void onDrop(String reason);
    /**
     * when receiving a text, this event is triggered
     * @param text text message from other user
     * */
    void onText(String text);
    /**
     * When a service request is received, this event is triggered
     * @param ringMessage ring message content
     * */
    void onRing(String ringMessage);
    /**
     * When a service request is cancelled, this event is triggered
     * */
    void onCancelRing();
    /**
     * When an error occurs in the current service, this event is triggered
     * @param errorCode error code
     * */
    void onError(int errorCode);
    /**
     * if the request for form data is successful, this event is triggered
     * @param formListDataList form data for current groups
     * */
    void onFormData(FormListData formListDataList);
    /**
     * new user joined current session
     * @param userInfo joined user info
     * */
    void onUserJoined(UserInfo userInfo);
    /**
     * user exit current session
     * @param userInfo exit user info
     * */
    void onUserQuit(UserInfo userInfo);
    /**
     * when custom message receive,this event is triggered
     * @param customMessage exit user info
     * */
    void onCustomMessage(CustomMessage customMessage);
    /**
     * when request submit form data success,this event is triggered
     * @param formData customer submit data
     * */
    void onSubmitFormData(FormData formData);

    void onHandWriting(String json);

    void compareFace(String result);
}
