package com.zealcomm.ivcs.agent;

public interface IvcsAgentCallback {
    void onSuccess();
    void onFailed(int errorCode,String error);
}
