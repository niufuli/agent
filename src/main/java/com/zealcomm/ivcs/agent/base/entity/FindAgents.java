package com.zealcomm.ivcs.agent.base.entity;

import java.util.List;

public class FindAgents {

    private String page;
    private String pageSize = "60";
    private String agentId;
    // ready 已就绪
    // notReady 未就绪
    // ringing 振铃中
    // serving 服务中
    // otherWork 小休
    private String agentStatus;
    private List<Integer> groups;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentStatus() {
        return agentStatus;
    }

    public void setAgentStatus(String agentStatus) {
        this.agentStatus = agentStatus;
    }

    public List<Integer> getGroups() {
        return groups;
    }

    public void setGroups(List<Integer> groups) {
        this.groups = groups;
    }
}
