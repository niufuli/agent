package com.zealcomm.ivcs.agent.base.https;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.zealcomm.base.RetrofitUtils;
import com.zealcomm.base.ServerCallback;
import com.zealcomm.base.entity.LoginReq;
import com.zealcomm.base.entity.RecordBody;
import com.zealcomm.ivcs.agent.base.entity.FindAgents;
import com.zealcomm.ivcs.agent.base.entity.MonitorToken;
import com.zealcomm.ivcs.agent.base.entity.Recordings;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentHttp {

    private String TAG = this.getClass().getName();
    public Context mContext;
    private AgentApi mAgentApi;

    public void init(Context context, String url) {
        mContext = context.getApplicationContext();
        if (url.contains("demo")) {
            mAgentApi = RetrofitUtils.createApi(context, AgentApiDemo.class, url);
        } else {
            mAgentApi = RetrofitUtils.createApi(context, AgentApiShowcase.class, url);
        }
    }

    public void getFormInfo(@NonNull String token , @NonNull String sessionId, ServerCallback callback) {
        getResult(callback, mAgentApi.getFormInfo(token , sessionId));
    }

    public void login(@NonNull String userName, @NonNull String password, @NonNull String agency, @NonNull String role, ServerCallback callback) {
        LoginReq body = new LoginReq();
        body.setUserName(userName);
        body.setPwd(password);
        body.setOrg(agency);
        body.setRole(role);
        getResult(callback, mAgentApi.login(body));
    }

    /**
     * @deprecated 由于 Android 权限问题，直接传路径不一定能拿到文件
     * @param url
     * @param filePath
     * @param callback
     */
    public void uploadSignature(@NonNull String url, @NonNull String filePath, ServerCallback callback) {
        File file = new File(filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        getResult(callback, mAgentApi.uploadSignatureImg(url + "ivcs/api/v1/upload", body));
    }

    public void uploadFile(@NonNull String url, @NonNull File file , ServerCallback callback) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        getResult(callback, mAgentApi.uploadSignatureImg(url + "ivcs/api/v1/upload", body));
    }

    public void getGroupIds(@NonNull String token, ServerCallback callback) {
        getResult(callback, mAgentApi.getGroups(token));
    }

    public void getGroupsDetailWithName(@NonNull String token, @NonNull String id, ServerCallback callback) {
        getResult(callback, mAgentApi.getGroupsDetailWithName(token, id));
    }

    public void getAllGroups(@NonNull String token, ServerCallback callback) {
        getResult(callback, mAgentApi.getAllGroups(token));
    }

    public void startRecord(@NonNull String token, @NonNull String roomId, String audioStreamId, String videoStreamId, ServerCallback callback) {
        RecordBody recordBody = new RecordBody();
        recordBody.setContainer("auto");
        RecordBody.MediaBean mediaBean = new RecordBody.MediaBean();
        RecordBody.MediaBean.AudioBean audioBean = new RecordBody.MediaBean.AudioBean();
        audioBean.setFrom(audioStreamId);
        RecordBody.MediaBean.VideoBean videoBean = new RecordBody.MediaBean.VideoBean();
        videoBean.setFrom(videoStreamId);
        mediaBean.setAudio(audioBean);
        mediaBean.setVideo(videoBean);
        recordBody.setMedia(mediaBean);
        getResult(callback, mAgentApi.startRecord(token, roomId, recordBody));
    }

    public void stopRecord(@NonNull String token, @NonNull String roomId, String recordId, ServerCallback callback) {
        getResult(callback, mAgentApi.stopRecord(token, roomId, recordId));
    }

    public void compareFace(@NonNull String token , @NonNull String userId , ServerCallback callback){
        getResult(callback , mAgentApi.compareFace(token , userId));
    }

    public void isChief(@NonNull String token , @NonNull String userId , @NonNull String groupId ,ServerCallback callback){
        getResult(callback , mAgentApi.isChief(token , userId , groupId));
    }

    public void findAgents(@NonNull String token , @NonNull FindAgents findAgents , ServerCallback callback){
        getResult(callback , mAgentApi.findAgents(token , findAgents));
    }

    public void getMonitorToken(@NonNull String token , @NonNull MonitorToken monitorToken , ServerCallback callback){
        getResult(callback , mAgentApi.getMonitorToken(token , monitorToken));
    }

    public void recordings(String mToken, String roomId, Recordings recordings, ServerCallback serverCallback) {
        getResult(serverCallback , mAgentApi.recordings(mToken , roomId , recordings));
    }

    public void record(String mToken , String sessionId , ServerCallback callback){
        getResult(callback, mAgentApi.record(mToken , sessionId));
    }

    public void getResult(final ServerCallback callback, Call<ResponseBody> call) {
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null) {
                        callback.successResponse(response.body().string());
                    } else if (response.errorBody() != null) {
                        Log.e(TAG, "getResult body null");
                        callback.failedResponse(response.errorBody().string());
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getResult error " + e.getMessage());
                    callback.failedResponse(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "getResult Failure " + t.getMessage());
                callback.failedResponse(t.getMessage());
            }
        });
    }


}
