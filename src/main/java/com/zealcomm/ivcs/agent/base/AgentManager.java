package com.zealcomm.ivcs.agent.base;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.zealcomm.Reason;
import com.zealcomm.ams.AmsClient;
import com.zealcomm.ams.AmsMessage;
import com.zealcomm.app.util.ExceptionUtil;
import com.zealcomm.app.util.GsonUtil;
import com.zealcomm.app.util.LogUtil;
import com.zealcomm.base.BaseManager;
import com.zealcomm.base.Constants;
import com.zealcomm.base.HttpErrorCode;
import com.zealcomm.base.JWTUtils;
import com.zealcomm.base.MessageData;
import com.zealcomm.base.ServerCallback;
import com.zealcomm.base.TLSSocketFactory;
import com.zealcomm.base.entity.AgentLoginRes;
import com.zealcomm.base.entity.CustomMessage;
import com.zealcomm.base.entity.GroupData;
import com.zealcomm.base.entity.HandWritingDataWrapper;
import com.zealcomm.base.entity.LinkData;
import com.zealcomm.base.entity.LoginData;
import com.zealcomm.base.entity.LoginError;
import com.zealcomm.base.entity.MediaOptions;
import com.zealcomm.base.entity.Profile;
import com.zealcomm.base.entity.SessionMessage;
import com.zealcomm.base.entity.StatusMsg;
import com.zealcomm.base.entity.UploadImageRes;
import com.zealcomm.ccs.CcsClient;
import com.zealcomm.ccs.CcsMessage;
import com.zealcomm.ivcs.agent.IvcsAgentCallback;
import com.zealcomm.ivcs.agent.IvcsAgentEvent;
import com.zealcomm.ivcs.agent.base.entity.AgentListInfoData;
import com.zealcomm.ivcs.agent.base.entity.AmsLoginData;
import com.zealcomm.ivcs.agent.base.entity.AmsRingMsg;
import com.zealcomm.ivcs.agent.base.entity.CollectedCustomerInfo;
import com.zealcomm.ivcs.agent.base.entity.Command;
import com.zealcomm.ivcs.agent.base.entity.CustomerInfoData;
import com.zealcomm.ivcs.agent.base.entity.FindAgents;
import com.zealcomm.ivcs.agent.base.entity.FormData;
import com.zealcomm.ivcs.agent.base.entity.FormListData;
import com.zealcomm.ivcs.agent.base.entity.HoldOnData;
import com.zealcomm.ivcs.agent.base.entity.InputForm;
import com.zealcomm.ivcs.agent.base.entity.InviteData;
import com.zealcomm.ivcs.agent.base.entity.MonitorToken;
import com.zealcomm.ivcs.agent.base.entity.OrgGroupData;
import com.zealcomm.ivcs.agent.base.entity.Recordings;
import com.zealcomm.ivcs.agent.base.entity.TokenBody;
import com.zealcomm.ivcs.agent.base.entity.UserInfo;
import com.zealcomm.ivcs.agent.base.https.AgentHttp;
import com.zealcomm.zms.BaseObserver;
import com.zealcomm.zms.SubSuccessData;
import com.zealcomm.zms.ZmsClient;
import com.zealcomm.zms.ZmsMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.SurfaceViewRenderer;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import irtc.base.MediaConstraints;
import irtc.base.VideoCodecParameters;
import irtc.base.VideoEncodingParameters;
import irtc.conference.Participant;
import irtc.conference.PublishOptions;
import irtc.conference.RemoteStream;

import static irtc.base.MediaCodecs.VideoCodec.H264;
import static irtc.base.MediaCodecs.VideoCodec.VP8;
import static irtc.base.MediaCodecs.VideoCodec.VP9;


public class AgentManager extends BaseManager implements AmsClient.AmsEventMessage,
        CcsClient.CcsEventMessage,
        ZmsClient.ZmsEventMessage,
        BaseObserver.BaseObserverCallback {

    public AmsRingMsg ringMsg;
    private HashMap<String, SurfaceViewRenderer> renderList = new HashMap<>();
    private HashMap<String, RemoteStream> remoteStreamList = new HashMap<>();
    private List<SubSuccessData> subSuccessDataList = new ArrayList<>();
    private BaseObserver mBaseObserver;
    private AgentLoginRes mLoginRes;
    private final String OK_EVENT = "ok";
    private AgentHttp agentHttp;
    String mRoomToken, mSessionId, mRoomId;
    private IvcsAgentCallback mIvcsAgentCallback;
    List<String> groupIds = new ArrayList<>();
    private IvcsAgentEvent mIvcsAgentEvent;
    private List<GroupData> groupIdList;
    private OrgGroupData allGroupIdList;
    private MediaOptions mediaOptions;

    // 用于强插时的信息处理
    private boolean isForceConnectCcs = false;
    private String forceSessionId = "";
    private AgentListInfoData.Data selectedAgent;
    private String baseUrl;

    public AgentManager(Context context) {
        mContext = context;
        TLSSocketFactory.init();
        AmsClient.getInstance().init(mContext, this);
        CcsClient.getInstance().init(mContext, this);
        ZmsClient.getInstance().init(mContext, this);
        agentHttp = new AgentHttp();
        mBaseObserver = new BaseObserver(this);
    }

    public void registerEvent(IvcsAgentEvent ivcsAgentEvent) {
        mIvcsAgentEvent = ivcsAgentEvent;
    }

    public void init(String backendUrl, String userName, String password, String org, MediaOptions mediaOptions, InitCallBack initCallBack) {
        this.mediaOptions = mediaOptions;
        this.baseUrl = backendUrl ;
        mInitCallback = initCallBack;
        agentHttp.init(mContext, backendUrl);
        mExecutorService = new ThreadPoolExecutor(1, 1, DURATION, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        mExecutorService.execute(() -> agentHttp.login(userName, password, org, Constants.STREAM_TYPE_AGENT, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                mLoginRes = new Gson().fromJson(result, AgentLoginRes.class);
                switch (mLoginRes.getCode()) {
                    case Constants.HTTP_REQUEST_SUCCESS_CODE:
                        mName = userName;
                        mAgency = org;
                        mToken = mLoginRes.getData().getAccessToken();
                        getGroupIds();
                        break;
                    default:
                        mInitCallback.initFailed(mLoginRes.getCode(), mLoginRes.getMessage());
                        break;
                }
            }

            @Override
            public void failedResponse(String result) {
                LoginError loginError = GsonUtil.toObject(result, LoginError.class);
                if (loginError != null) {
                    mInitCallback.initFailed(Integer.valueOf(loginError.getCode()), loginError.getMessage());
                } else {
                    mInitCallback.initFailed(HttpErrorCode.NETWORK_ERROR, HttpErrorCode.NETWORK_REQUEST_FAILED);
                }
            }
        }));
    }

    private void getGroupIds() {
        mExecutorService.execute(() -> agentHttp.getGroupIds(mToken, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                groupIds.clear();
                groupIdList = new Gson().fromJson(result, new TypeToken<List<GroupData>>() {
                }.getType());
                if (groupIdList != null) {
                    for (GroupData dataBean : groupIdList) {
                        groupIds.add(dataBean.getGroup().getName());
                    }
                    getAllGroups();
                }
                mInitCallback.initSuccess(mToken, groupIds, groupIdList);
                AmsClient.getInstance().connect(mToken, mLoginRes.getData().getUrls().getAmsurl());
            }

            @Override
            public void failedResponse(String result) {
                AmsClient.getInstance().connect(mToken, mLoginRes.getData().getUrls().getAmsurl());
            }
        }));
    }

    List<String> nameList = new ArrayList<>();

    public List<String> getAllGroupNames() {
        return nameList;
    }

    private void getAllGroups() {
        mExecutorService.execute(() -> agentHttp.getAllGroups(mToken, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                nameList.clear();
                allGroupIdList = new Gson().fromJson(result, OrgGroupData.class);
                if (allGroupIdList != null) {
                    for (OrgGroupData.DataBean dataBean : allGroupIdList.getData()) {
                        nameList.add(dataBean.getName());
                    }
                }

            }

            @Override
            public void failedResponse(String result) {

            }
        }));
    }

    public void compareFace() {
        mExecutorService.execute(() -> agentHttp.compareFace(mToken, isForceConnectCcs ? selectedAgent.getInvites().get(0).getFromId() : ringMsg.getUserData().getUserId(), new ServerCallback() {
            @Override
            public void successResponse(String result) throws Exception {
                mIvcsAgentEvent.compareFace(result);
            }

            @Override
            public void failedResponse(String result) {
                mIvcsAgentEvent.onError(HttpErrorCode.CCS_COMPARE_FACE_ERROR);
            }
        }));
    }

    public void isChief(String userId, String groupId, IvcsAgentCallback ivcsAgentCallback) {
        mExecutorService.execute(() -> agentHttp.isChief(mToken, userId, groupId, new ServerCallback() {
            @Override
            public void successResponse(String result) throws Exception {
                if ("true".equals(result)) {
                    ivcsAgentCallback.onSuccess();
                } else {
                    ivcsAgentCallback.onFailed(-1, result);
                }
            }

            @Override
            public void failedResponse(String result) {
                ivcsAgentCallback.onFailed(-1, result);
            }
        }));
    }

    public void login(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            AmsClient.getInstance().login();
        });
    }

    private List<Integer> getGroupIdList(List<String> groupNames) {
        List<Integer> ids = new ArrayList<>();
        for (String name : groupNames) {
            for (GroupData item : groupIdList) {
                if (name.equals(item.getGroup().getName())) {
                    // TOD
                    // O showcase 环境下不要用 by nfl 2021.05.27
                    // ids.add(item.getGroupId());
                    ids.add(item.getGroup().getId());
                }
            }
        }
        return ids;
    }

    private String getGroupId(String groupNames) {
        String ids = null;
        for (OrgGroupData.DataBean item : allGroupIdList.getData()) {
            if (groupNames.equals(item.getName())) {
                ids = item.get_id();
            }
        }
        return ids;
    }

    public void checkIn(List<String> groupIds, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            AmsLoginData amsLoginData = new AmsLoginData();
            if (groupIds != null && groupIds.size() > 0) {
                amsLoginData.setGroups(getGroupIdList(groupIds));
            }
            JSONObject services = null;
            try {
                services = new JSONObject(new Gson().toJson(amsLoginData));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            AmsClient.getInstance().checkIn(services);
        });
    }

    public void checkOut(List<String> groupIds, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            AmsLoginData amsLoginData = new AmsLoginData();
            if (groupIds != null && groupIds.size() > 0) {
                amsLoginData.setGroups(getGroupIdList(groupIds));
            }
            JSONObject services = null;
            try {
                services = new JSONObject(new Gson().toJson(amsLoginData));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            AmsClient.getInstance().checkOut(services);
        });
    }

    public void logout() {
        mExecutorService.execute(() -> {
            AmsClient.getInstance().logout();
        });
    }

    public void obtainCustomerInfo(IvcsAgentCallback agentCallback, CustomerInfoData data) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            SessionMessage textMessage = new SessionMessage();
            textMessage.setData(data);
            sendMsg(textMessage, CcsMessage.CCS_FORM, null);
        });
    }

    public void ready(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            AmsClient.getInstance().changeStatus(AmsMessage.AGENT_READY);
        });
    }

    public void unReady(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            AmsClient.getInstance().changeStatus(AmsMessage.AGENT_UNREADY);
        });
    }

    public void otherWork(String reason, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            Reason mReason = new Reason();
            mReason.setReason(reason);
            AmsClient.getInstance().changeStatus(AmsMessage.AGENT_OTHERWORK, mReason);
        });
    }

    public void appoint(String state, String reason, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            Reason mReason = new Reason();
            mReason.setReason(reason);
            mReason.setAppointState(state);
            AmsClient.getInstance().changeStatus(AmsMessage.AGENT_APPOINT, mReason);
        });
    }

    public void reject(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            AmsClient.getInstance().changeStatus(AmsMessage.AGENT_REJECT);
        });
    }

    public void answer(String videoLabel, IvcsAgentCallback agentCallback) {
        mStreamType = videoLabel;
        mIvcsAgentCallback = agentCallback;
        if (ringMsg != null) {
            mExecutorService.execute(() -> {
                AmsClient.getInstance().changeStatus(AmsMessage.AGENT_ANSWER);
            });
        }
    }

    public void muteLocalStreamAudio(boolean isMute) {
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().setLocalAudio(isMute);
        });
    }

    public void muteLocalStreamVideo(boolean isMute) {
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().setLocalVideo(isMute);
        });
    }

    public void hangup(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().stop();
            CcsClient.getInstance().quitSession();
            AmsClient.getInstance().hangup();
        });
    }

    public void quitSession(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().stop();
            // 这个方法会触发 ivcsAgentEvent.onDrop("ccs")
            CcsClient.getInstance().quitSession();
            // 这个会触发 mIvcsAgentCallback.XXX
            AmsClient.getInstance().hangup();
        });
    }

    public void holdOn(IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            SessionMessage textMessage = new SessionMessage();
            textMessage.setData(new HoldOnData());
            sendMsg(textMessage, CcsMessage.CCS_PEER_STATUS, null);
        });
    }

    public void registerRender(String type, SurfaceViewRenderer renderer) {
        if (type.equals(LOCAL_RENDER)) {
            ZmsClient.getInstance().setRenderer(renderer);
        }
        renderList.put(type, renderer);
        RemoteStream remoteStream = remoteStreamList.get(type);
        if (remoteStream != null && remoteStream.hasVideo()) {
            remoteStream.attach(renderer);
        }
    }

    public void unregisterRender(String type, SurfaceViewRenderer renderer) {
        renderList.remove(type);
        RemoteStream remoteStream = remoteStreamList.get(type);
        if (remoteStream != null && remoteStream.hasVideo() && renderer != null) {
            remoteStream.detach(renderer);
        }
    }

    public void sendText(String text, String recipient, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        SessionMessage textMessage = new SessionMessage();
        textMessage.setData(text);
        sendMsg(textMessage, CcsMessage.CCS_TEXT, recipient);
    }

    public void sendCoustomMessage(CustomMessage customMessage, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        SessionMessage textMessage = new SessionMessage();
        textMessage.setData(customMessage);
        sendMsg(textMessage, CcsMessage.CCS_CUSTOM_MESSAGE, customMessage.getReceiver() != null ? customMessage.getReceiver() : Constants.CLIENT_AGENT);
    }

    public void sendLink(String url, String recipient, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        LinkData linkData = new LinkData();
        linkData.setUrl(url);
        linkData.setName("");
        linkData.setType(CcsMessage.LINK_PAGE);
        SessionMessage textMessage = new SessionMessage();
        textMessage.setData(linkData);
        sendMsg(textMessage, CcsMessage.CCS_LINK, recipient);
    }

    public void sendSanpshot(String recipient, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        SessionMessage statusMsg = new SessionMessage();
        Command command = new Command();
        command.setAskForPermission("false");
        command.setCmd(CcsMessage.CCS_SCREEN_SHOT);
        statusMsg.setData(command);
        sendMsg(statusMsg, CcsMessage.CCS_PEER_CMD, recipient);
    }

    public void sendPhotoGuide(String recipient, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        SessionMessage statusMsg = new SessionMessage();
        Command command = new Command();
        command.setAskForPermission("false");
        command.setCmd(CcsMessage.PHOTO_GUIDEBOX);
        statusMsg.setData(command);
        sendMsg(statusMsg, CcsMessage.CCS_PEER_CMD, recipient);
    }

    public void sendSign(String recipient, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        SessionMessage textMessage = new SessionMessage();
        textMessage.setData(CcsMessage.CCS_HANDSIGN);
        sendMsg(textMessage, CcsMessage.CCS_HANDWRITING, recipient);
    }

    public void getFormData() {
        Log.i(TAG, "getFormData");
        mExecutorService.execute(() -> agentHttp.getGroupsDetailWithName(mToken, isForceConnectCcs ? selectedAgent.getGroups().get(0) + "" : ringMsg.getSession().getGroup(), new ServerCallback() {
            @Override
            public void successResponse(String result) {
                FormListData formListData = new Gson().fromJson(result, FormListData.class);
                mIvcsAgentEvent.onFormData(formListData);
            }

            @Override
            public void failedResponse(String result) {

            }
        }));
    }

    public void getSubmitFormData() {
        mExecutorService.execute(() -> agentHttp.getFormInfo(mToken, mSessionId, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                FormData formData = new Gson().fromJson(result, FormData.class);
                mIvcsAgentEvent.onSubmitFormData(formData);
            }

            @Override
            public void failedResponse(String result) {

            }
        }));
    }


    public void sendFormCmd(String form, String recipient, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        SessionMessage statusMsg = new SessionMessage();
        InputForm inputForm = new InputForm();
        inputForm.setInputform(1);
        JsonObject example = new Gson().fromJson(form, JsonObject.class);
        inputForm.setFormTemplate(example);
        statusMsg.setData(inputForm);
        sendMsg(statusMsg, CcsMessage.CCS_FORM, recipient);
    }

    public void inviteThrid(String name, String services, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        InviteData inviteData = new InviteData();
        inviteData.setAgentId(name);
        inviteData.setService(getGroupId(services));
        JSONObject message = null;
        try {
            message = new JSONObject(new Gson().toJson(inviteData));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CcsClient.getInstance().invite(message);
    }

    public void transfer(String name, String services, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        InviteData inviteData = new InviteData();
        inviteData.setAgentId(name);
        inviteData.setService(getGroupId(services));
        JSONObject message = null;
        try {
            message = new JSONObject(new Gson().toJson(inviteData));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CcsClient.getInstance().invite(message);
    }

    public void switchCamera() {
        ZmsClient.getInstance().switchCamera();
    }

    /**
     * @param path
     * @param userRole
     * @param name
     * @param fileType
     * @param ivcsAgentCallback
     * @deprecated 由于 Android 权限问题，直接传路径不一定能获取到文件
     */
    @Deprecated
    public void sendFile(String path, String userRole, String name, String fileType, IvcsAgentCallback ivcsAgentCallback) {
        mIvcsAgentCallback = ivcsAgentCallback;
        // mLoginRes.getData().getUrls().getUploadUrl() 改成 baseUrl
        mExecutorService.execute(() -> agentHttp.uploadSignature(baseUrl , path, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                UploadImageRes uploadImageRes = new Gson().fromJson(result, UploadImageRes.class);
                if (uploadImageRes.getCode() == Constants.HTTP_REQUEST_SUCCESS_CODE) {
                    LinkData linkData = new LinkData();
                    linkData.setUrl(uploadImageRes.getData().getUrl());
                    linkData.setType(fileType);
                    linkData.setName(name);
                    SessionMessage linkMessage = new SessionMessage();
                    linkMessage.setType(CcsMessage.CCS_LINK);
                    linkMessage.setFrom(mSessionId);
                    linkMessage.setData(linkData);
                    linkMessage.setId(new Random().nextInt() + "");
                    linkMessage.setTo(userRole != null ? userRole : Constants.CLIENT_CUSTOMERS);
                    JSONObject message = null;
                    try {
                        message = new JSONObject(new Gson().toJson(linkMessage));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CcsClient.getInstance().sendMessage(message, fileType);
                } else {
                    mIvcsAgentCallback.onFailed(uploadImageRes.getCode(), result);
                }
            }

            @Override
            public void failedResponse(String result) {
                mIvcsAgentCallback.onFailed(HttpErrorCode.NETWORK_ERROR, HttpErrorCode.NETWORK_REQUEST_FAILED);
            }
        }));
    }

    public void sendFile(File file, String userRole, String name, String fileType, IvcsAgentCallback ivcsAgentCallback) {
        mIvcsAgentCallback = ivcsAgentCallback;
        // mLoginRes.getData().getUrls().getUploadUrl() 改成 baseUrl
        mExecutorService.execute(() -> agentHttp.uploadFile(baseUrl, file, new ServerCallback() {
            @Override
            public void successResponse(String result) {
                UploadImageRes uploadImageRes = new Gson().fromJson(result, UploadImageRes.class);
                if (uploadImageRes.getCode() == Constants.HTTP_REQUEST_SUCCESS_CODE) {
                    LinkData linkData = new LinkData();
                    linkData.setUrl(uploadImageRes.getData().getUrl());
                    linkData.setType(fileType);
                    linkData.setName(name);
                    SessionMessage linkMessage = new SessionMessage();
                    linkMessage.setType(CcsMessage.CCS_LINK);
                    linkMessage.setFrom(mSessionId);
                    linkMessage.setData(linkData);
                    linkMessage.setId(new Random().nextInt() + "");
                    linkMessage.setTo(userRole != null ? userRole : Constants.CLIENT_CUSTOMERS);
                    JSONObject message = null;
                    try {
                        message = new JSONObject(new Gson().toJson(linkMessage));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_LINK);
                } else {
                    mIvcsAgentCallback.onFailed(uploadImageRes.getCode(), result);
                }
            }

            @Override
            public void failedResponse(String result) {
                mIvcsAgentCallback.onFailed(HttpErrorCode.NETWORK_ERROR, HttpErrorCode.NETWORK_REQUEST_FAILED);
            }
        }));
    }

    public void shareScreen(Intent data, IvcsAgentCallback agentCallback) {
        mIvcsAgentCallback = agentCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().screenShare(data, DEFAULT_PUBLISH_VIDEO_WIDTH, DEFAULT_PUBLISH_VIDEO_HEIGHT);
        });
    }

    public void stopShareScreen() {
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().stopScreen();
        });
    }

    private void sendMsg(SessionMessage sessionMessage, String type, String recipient) {
        sessionMessage.setFrom(mSessionId);
        sessionMessage.setId(new Random().nextInt() + "");
        sessionMessage.setTo(recipient != null ? recipient : Constants.CLIENT_CUSTOMERS);
        sessionMessage.setType(type);
        JSONObject message = null;
        try {
            message = new JSONObject(new Gson().toJson(sessionMessage));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        CcsClient.getInstance().sendMessage(message, type);
    }

    public EglBase.Context getEGLContext() {
        return ZmsClient.getInstance().getEGLContext();
    }

    @Override
    public void onAmsEvent(MessageData messageData) {
        LogUtil.i("allMessage ams message:" + GsonUtil.object2String(messageData));
        switch (messageData.getType()) {
            case AmsMessage.AMS_CONNECT_SUCCESS:
                Log.i(TAG, "AMS_CONNECT_SUCCESS");
                // TODO 这里注销掉 by nfl 2021.05.26.18.00
                // login();
                break;
            case AmsMessage.AMS_LOGIN_SUCCESS:
            case AmsMessage.AMS_CHECK_OUT_SUCCESS:
            case AmsMessage.AGENT_HANGUP_SUCCESS:
            case AmsMessage.AMS_CHECK_IN_SUCCESS:
                // mInitCallback.initSuccess(mToken, groupIds);
                mIvcsAgentCallback.onSuccess();
                break;
            // TODO 目前退出客户系统没有回调
            case AmsMessage.AMS_LOGIN_OUT_SUCCESS:
                // mIvcsAgentCallback.onSuccess();
                break;
            case AmsMessage.AMS_LOGIN_OUT_FAILED:
                // mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_QUIT_ERROR , HttpErrorCode.AMS_QUIT_ERROR_MESSAGE);
                break;
            case AmsMessage.AMS_CONNECT_FAILED:
                mIvcsAgentEvent.onError(HttpErrorCode.AMS_CONNECT_ERROR);
                break;
            case AmsMessage.AMS_LOGIN_FAILED:
                mInitCallback.initFailed(HttpErrorCode.AMS_LOGIN_ERROR, HttpErrorCode.AMS_SERVER_CONNECT_ERROR);
                break;
            case AmsMessage.AGENT_READY:
            case AmsMessage.AGENT_UNREADY:
                String readyStatus = (String) messageData.getData();
                if (readyStatus.equals(OK_EVENT)) {
                    mIvcsAgentCallback.onSuccess();
                } else {
                    mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_READY_ERROR, HttpErrorCode.AMS_READY_ERROR_MESSAGE);
                }
                break;
            case AmsMessage.AMS_RING:
                JSONObject jsonObject = (JSONObject) messageData.getData();
                Log.i(TAG, "ring = " + jsonObject.toString());
                ringMsg = GsonUtil.toObject(jsonObject.toString(), AmsRingMsg.class);
                mIvcsAgentEvent.onRing(ringMsg.getSession().getGroup());
                break;
            case AmsMessage.AMS_STOP_RING:
                mIvcsAgentEvent.onCancelRing();
                break;
            case AmsMessage.AMS_DROP:
                mIvcsAgentEvent.onDrop("ams drop");
                break;
            case AmsMessage.AMS_REPLACED:
                mIvcsAgentEvent.onDrop("replaced");
                break;
            case AmsMessage.AGENT_ANSWER:
                String answerStatus = (String) messageData.getData();
                if (answerStatus.equals(OK_EVENT)) {
                    CcsClient.getInstance().connect(mToken,
                            getHost(ringMsg.getSession().getUrl().getPublicUrl()),
                            ringMsg.getSession().getUrl().getPublicUrl()
                                    .substring(getHost(ringMsg.getSession().getUrl().getPublicUrl()).length() + 1));
                } else {
                    mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_ANSWER_ERROR, HttpErrorCode.AMS_ANSWER_ERROR_MESSAGE);
                }
                break;

            case AmsMessage.AGENT_OTHERWORK:
            case AmsMessage.AGENT_APPOINT:
                String otherWorkStatus = (String) messageData.getData();
                if (otherWorkStatus.equals(OK_EVENT)) {
                    mIvcsAgentCallback.onSuccess();
                } else {
                    mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_ERROR, otherWorkStatus);
                }
                break;
            case AmsMessage.AMS_CHECK_IN_FAILED:
                mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_CHECK_IN_ERROR, HttpErrorCode.AMS_CHECK_IN_ERROR_MESSAGE);
                break;
            case AmsMessage.AGENT_HANGUP_FAILED:
                mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_HANGUP_ERROR, HttpErrorCode.AMS_HANGUP_ERROR_MESSAGE);
                break;
            case AmsMessage.AMS_CHECK_OUT_FAILED:
                mIvcsAgentCallback.onFailed(HttpErrorCode.AMS_CHECK_OUT_ERROR, HttpErrorCode.AMS_CHECK_OUT_ERROR_MESSAGE);
                break;
            default:
                break;
        }
    }

    public static String getHost(String url) {
        String re = "((http|ftp|https)://)(([a-zA-Z0-9._-]+)|([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}))(([a-zA-Z]{2,6})|(:[0-9]{1,4})?)";
        String str = "";
        Pattern pattern = Pattern.compile(re);
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            str = url;
        } else {
            String[] split2 = url.split(re);
            if (split2.length > 1) {
                String substring = url.substring(0, url.length() - split2[1].length());
                str = substring;
            } else {
                str = split2[0];
            }
        }
        return str;
    }

    @Override
    public void onCcsEvent(MessageData messageData) {
        LogUtil.i("allMessage ccs message:" + GsonUtil.object2String(messageData));
        switch (messageData.getType()) {
            case CcsMessage.CCS_CONNECT_SUCCESS:
                mExecutorService.execute(() -> {
                    LoginData loginData = new LoginData();
                    loginData.setMedia(MEDIA);
                    LoginData.ClientInfoBean clientInfoBean = new LoginData.ClientInfoBean();
                    clientInfoBean.setType(CLIENT_INFO);
                    loginData.setClientInfo(clientInfoBean);
                    if (isForceConnectCcs) {
                        loginData.setSessionId(forceSessionId);
                    } else {
                        loginData.setSessionId(ringMsg.getSession().getId());
                    }
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(new Gson().toJson(loginData));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.i(TAG, "ccsLogin data = " + jsonObject.toString());
                    CcsClient.getInstance().ccsLogin(jsonObject);
                });
                break;
            case CcsMessage.CCS_LOGIN_SUCCESS:
                mExecutorService.execute(() -> {
                    JSONObject data = (JSONObject) messageData.getData();
                    try {
                        mRoomToken = data.getString("roomToken");
                        mSessionId = data.getString("sessionId");
                        mRoomId = data.getString("roomId");
                        ZmsClient.getInstance().joinRoom(mRoomToken);
                        SessionMessage statusMsg = new SessionMessage();
                        statusMsg.setType(Constants.PEER_STATUS);
                        statusMsg.setFrom(mSessionId);
                        statusMsg.setId(new Random().nextInt() + "");
                        statusMsg.setTo(Constants.USER_CUSTOMERS);
                        StatusMsg ass = new StatusMsg();
                        ass.setAt(new Date().toString());
                        ass.setStatus(Constants.READY_TO_TALK);
                        ass.setParticipantId(ZmsClient.getInstance().getSelfId());
                        Profile profile = new Profile();
                        ass.setUser(profile);
                        statusMsg.setData(ass);
                        JSONObject message = null;
                        try {
                            message = new JSONObject(new Gson().toJson(statusMsg));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        CcsClient.getInstance().sendMessage(message, CcsMessage.CCS_READY_TO_TALK);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
                break;
            case CcsMessage.CCS_DROP:
                isForceConnectCcs = false;
                mExecutorService.execute(() -> {
                    Log.e(TAG, "drop ccs");
                    ZmsClient.getInstance().drop();
                    AmsClient.getInstance().changeStatus(AmsMessage.AGENT_HANGUP);
                    mIvcsAgentEvent.onDrop("ccs");
                });
                break;
            case CcsMessage.CCS_QUIT_SESSION:
                isForceConnectCcs = false;
                mIvcsAgentEvent.onDrop("ccs");
                break;
            case CcsMessage.CCS_TEXT:
                JSONObject message = (JSONObject) messageData.getData();
                if (message != null) {
                    try {
                        String text = message.getString("data");
                        Log.i(TAG, "text = " + text);
                        if (mIvcsAgentCallback != null) {
                            mIvcsAgentEvent.onText(text);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CcsMessage.CCS_LINK:
                message = (JSONObject) messageData.getData();
                if (message != null) {
                    try {
                        JSONObject data = message.getJSONObject("data");
                        String linkType = data.getString("type");
                        String name = data.getString("name");
                        mIvcsAgentEvent.onResource(data.getString("url"), linkType, name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CcsMessage.CCS_SEND_LINK_SUCCESS:
            case CcsMessage.CCS_PEER_CMD:
            case CcsMessage.CCS_FORM:
            case CcsMessage.CCS_INVITE_SUCCESS:
            case CcsMessage.CCS_SEND_MESSAGE_SUCCESS:
                // 仅用于发送成功回调，不处理资源
                mIvcsAgentCallback.onSuccess();
                break;
            case CcsMessage.CCS_SUCCEED_TO_SEND_MESSAGE:
                String success_type = (String) messageData.getData();
                switch (success_type) {
                    case CcsMessage.CCS_READY_TO_TALK:
                        break;
                    default:
                        mIvcsAgentCallback.onSuccess();
                        break;
                }
                break;
            case CcsMessage.CCS_FAILED_TO_SEND_MESSAGE:
                mIvcsAgentCallback.onFailed(0, (String) messageData.getData());
                break;
            case CcsMessage.CCS_PEER_STATUS:
                message = (JSONObject) messageData.getData();
                Log.e("CCS_PEER_STATUS", message.toString());
                try {
                    JSONObject data = message.getJSONObject("data");
                    String status = data.getString("status");

                    UserInfo userInfo = new UserInfo();
                    try {
                        JSONObject user = data.getJSONObject("user");
                        userInfo.setOrg(user.getString("org"));
                        userInfo.setUserId(user.getString("id"));
                        userInfo.setName(user.getString("name"));
                        userInfo.setRole(data.getString("role"));
                    } catch (JSONException e) {
                        LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
                    }
                    switch (status) {
                        case CcsMessage.USER_JOINED:
                            mIvcsAgentEvent.onUserJoined(userInfo);
                            break;
                        case CcsMessage.USER_QUIT:
                            mIvcsAgentEvent.onUserQuit(userInfo);
                            break;
                        case CcsMessage.CCS_HOLD_ON:
                            mIvcsAgentCallback.onSuccess();
                            break;
                        default:
                            break;
                    }
                } catch (JSONException e) {
                    LogUtil.i(ExceptionUtil.getExceptionTraceString(e));
                }
                break;
            case CcsMessage.CCS_INVITE_FAILED:
                mIvcsAgentCallback.onFailed(HttpErrorCode.INVITE_USER_ERROR, HttpErrorCode.INVITE_USER_ERROR_MESSAGE);
                break;
            case CcsMessage.CCS_CUSTOM_MESSAGE:
                message = (JSONObject) messageData.getData();
                CustomMessage customMessage = new CustomMessage();
                try {
                    customMessage.setData(message.getString("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mIvcsAgentEvent.onCustomMessage(customMessage);
                break;
            case CcsMessage.CCS_HANDWRITING:
                // TODO 待实现签名功能
                message = (JSONObject) messageData.getData();
                if (message != null) {
                    HandWritingDataWrapper handWritingDataWrapper = GsonUtil.toObject(
                            GsonUtil.object2String(message), HandWritingDataWrapper.class);
                    if (handWritingDataWrapper != null && handWritingDataWrapper.getNameValuePairs() != null
                            && handWritingDataWrapper.getNameValuePairs().getData() != null
                    ) {
                        mIvcsAgentEvent.onHandWriting(handWritingDataWrapper.getNameValuePairs().getData());
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onZmsEvent(MessageData messageData) {
        LogUtil.i("allMessage zms message:" + GsonUtil.object2String(messageData));
        switch (messageData.getType()) {
            case ZmsMessage.ZMS_JOIN_SUCCESS:
                mExecutorService.execute(() -> {
                    PublishOptions options = null;
                    VideoEncodingParameters h264 = new VideoEncodingParameters(new VideoCodecParameters(H264), DEFAULT_VIDEO_MAX_BITRATE);
                    VideoEncodingParameters vp8 = new VideoEncodingParameters(new VideoCodecParameters(VP8), DEFAULT_VIDEO_MAX_BITRATE);
                    VideoEncodingParameters vp9 = new VideoEncodingParameters(new VideoCodecParameters(VP9), DEFAULT_VIDEO_MAX_BITRATE);
                    options = PublishOptions.builder()
                            .addVideoParameter(vp8)
                            .addVideoParameter(h264)
                            .addVideoParameter(vp9)
                            .build();
                    // TODO 先关闭推流
                    ZmsClient.getInstance().publish(mediaOptions != null ? mediaOptions.getVideoOptions().getVideoWidth() : DEFAULT_PUBLISH_VIDEO_WIDTH,
                            mediaOptions != null ? mediaOptions.getVideoOptions().getVideoFrameRate() : DEFAULT_PUBLISH_VIDEO_HEIGHT,
                            mediaOptions != null ? mediaOptions.getVideoOptions().getVideoHeight() : DEFAULT_PUBLISH_VIDEO_FPS,
                            mStreamType, options);
                });
                break;
            case ZmsMessage.ZMS_PUBLISH_SUCCESS:
                mIvcsAgentCallback.onSuccess();
                break;
            case ZmsMessage.ZMS_PUBLISH_FAILED:
                mIvcsAgentCallback.onFailed(HttpErrorCode.ZMS_ERROR, HttpErrorCode.ZMS_PUBLISH_ERROR_MESSAGE);
                break;
            case ZmsMessage.ZMS_SUBSCRIBE_SUCCESS:
                SubSuccessData subSuccessData = (SubSuccessData) messageData.getData();
                subSuccessDataList.add(subSuccessData);
                streamAdd(subSuccessData.getRemoteStream());
                mBaseObserver.addRemoteStream(subSuccessData.getRemoteStream());
                break;
            default:
                break;
        }
    }

    private void streamAdd(RemoteStream remoteStream) {
        if (remoteStream.getAttributes() != null) {
            String type = remoteStream.getAttributes().get(Constants.STREAM_TYPE);
            Log.e(TAG, "streamd add =" + type);
            SurfaceViewRenderer renderer = renderList.get(remoteStream.getAttributes().get(Constants.STREAM_TYPE));
            if (renderer != null) {
                Log.e(TAG, "streamd attach =" + type);
                remoteStream.attach(renderer);
            }
            remoteStreamList.put(type, remoteStream);
            mIvcsAgentEvent.onVideoLabel(type, true);
        }
    }

    /**
     * @param remoteStream mute all subscribe stream if remoteStream is null
     * @param isMute       true mute,false unmute;
     */
    public void muteStreamVideo(RemoteStream remoteStream, boolean isMute) {

        if (isMute) {
            for (SubSuccessData subSuccessData : subSuccessDataList) {
                if (remoteStream == null) {
                    subSuccessData.getRemoteStream().disableVideo();
                    subSuccessData.getSubscription().mute(MediaConstraints.TrackKind.VIDEO, null);
                } else if (remoteStream.id().equals(subSuccessData.getRemoteStream().id())) {
                    subSuccessData.getRemoteStream().disableVideo();
                    subSuccessData.getSubscription().mute(MediaConstraints.TrackKind.VIDEO, null);
                    break;
                }
            }
        } else {
            for (SubSuccessData subSuccessData : subSuccessDataList) {
                if (remoteStream == null) {
                    subSuccessData.getRemoteStream().enableVideo();
                    subSuccessData.getSubscription().unmute(MediaConstraints.TrackKind.VIDEO, null);
                } else if (remoteStream.id().equals(subSuccessData.getRemoteStream().id())) {
                    subSuccessData.getRemoteStream().enableVideo();
                    subSuccessData.getSubscription().unmute(MediaConstraints.TrackKind.VIDEO, null);
                    break;
                }
            }
        }
    }

    @Override
    public void streamEnd(RemoteStream remoteStream) {
        String type = null;
        for (Map.Entry<String, RemoteStream> entry : remoteStreamList.entrySet()) {
            if (remoteStream.id().equals(entry.getValue().id())) {
                type = entry.getKey();
                break;
            }
        }
        if (type != null) {
            mIvcsAgentEvent.onVideoLabel(type, false);
            SurfaceViewRenderer renderer = renderList.get(type);
            if (renderer != null) {
                remoteStreamList.get(type).detach(renderer);
                remoteStreamList.remove(type);
            }
        }
        mBaseObserver.removeStream(remoteStream);
    }

    @Override
    public void streamUpdate(RemoteStream remoteStream) {

    }

    @Override
    public void participantLeft(Participant participant) {

    }

    public void findAgents(FindAgents findAgents, ServerCallback callback) {
        agentHttp.findAgents(mToken, findAgents, callback);
    }

    public void getMonitorToken(String roomId, ServerCallback callback) {
        MonitorToken monitorToken = new MonitorToken();
        monitorToken.setUser(JWTUtils.decoded(mToken, TokenBody.class).getData().getUserName());
        monitorToken.setRoom(roomId);
        agentHttp.getMonitorToken(mToken, monitorToken, callback);
    }

    public void recordings(String roomId, Recordings recordings, ServerCallback serverCallback) {
        agentHttp.recordings(mToken, roomId, recordings, serverCallback);
    }

    public void record(String sessionId, ServerCallback serverCallback) {
        agentHttp.record(mToken, sessionId, serverCallback);
    }

    public void joinRoom(String roomToken, IvcsAgentCallback ivcsAgentCallback) {
        mIvcsAgentCallback = ivcsAgentCallback;
        mExecutorService.execute(() -> {
            ZmsClient.getInstance().joinRoom(roomToken);
        });
    }

    public void forceConnectCcs(String sessionId, String url, String query) {
        isForceConnectCcs = true;
        forceSessionId = sessionId;
        CcsClient.getInstance().connect(mToken, url, query);
    }

    public void updateMonitorAgentInfo(AgentListInfoData.Data selectedAgent) {
        this.selectedAgent = selectedAgent;
    }

    public void collectCustomerInfo(CollectedCustomerInfo collectedCustomerInfo, IvcsAgentCallback ivcsAgentCallback) {
        mIvcsAgentCallback = ivcsAgentCallback;
        SessionMessage textMessage = new SessionMessage();
        textMessage.setData(collectedCustomerInfo);
        sendMsg(textMessage, CcsMessage.CCS_FORM, null);
    }
}
