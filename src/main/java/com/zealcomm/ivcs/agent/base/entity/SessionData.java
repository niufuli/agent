package com.zealcomm.ivcs.agent.base.entity;

public class SessionData {


    /**
     * user : {"id":"\u2018\u2019","name":"\u2018\u2019","org":"\u2019\u2019"}
     * role : ’agent’
     * media : ‘video’
     * sessionId : ‘SessionId’
     * clientInfo : {"type":" "}
     */

    private UserBean user;
    private String role;
    private String media;
    private String sessionId;
    private ClientInfoBean clientInfo;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public ClientInfoBean getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfoBean clientInfo) {
        this.clientInfo = clientInfo;
    }

    public static class UserBean {
        /**
         * id : ‘’
         * name : ‘’
         * org : ’’
         */

        private String id;
        private String name;
        private String org;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }
    }

    public static class ClientInfoBean {
        /**
         * type :
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
