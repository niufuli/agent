package com.zealcomm.ivcs.agent.base.https;

import com.zealcomm.base.entity.LoginReq;
import com.zealcomm.base.entity.RecordBody;
import com.zealcomm.ivcs.agent.base.entity.FindAgents;
import com.zealcomm.ivcs.agent.base.entity.MonitorToken;
import com.zealcomm.ivcs.agent.base.entity.Recordings;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface AgentApi {

    /**
     * 获取访客提交的用户信息
     */
    Call<ResponseBody> getFormInfo(@Header("x-access-token") String token, @Path("sessionId") String sessionId);

    /**
     * 登录
     */
    Call<ResponseBody> login(@Body LoginReq tokenReq);

    /**
     * 提交图片
     */
    @Multipart
    @POST()
    Call<ResponseBody> uploadSignatureImg(@Url String url, @Part MultipartBody.Part part);

    Call<ResponseBody> getGroups(@Header("x-access-token") String token);

    Call<ResponseBody> getGroupsDetailWithName(@Header("x-access-token") String token, @Path("id") String id);

    Call<ResponseBody> startRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Body RecordBody recordBody);

    Call<ResponseBody> stopRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Path("recordId") String recordId);

    Call<ResponseBody> getAllGroups(@Header("x-access-token") String token);

    Call<ResponseBody> compareFace(@Header("x-access-token") String token, @Path("userId") String userId);

    Call<ResponseBody> isChief(@Header("x-access-token") String token, @Query("userId") String userId, @Query("groupId") String groupId);

    Call<ResponseBody> findAgents(@Header("x-access-token") String token, @Body FindAgents findAgents);

    Call<ResponseBody> getMonitorToken(@Header("x-access-token") String token, @Body MonitorToken monitorToken);

    Call<ResponseBody> recordings(@Header("x-access-token") String token, @Path("roomId") String roomId , @Body Recordings recordings);

    Call<ResponseBody> record(@Header("x-access-token") String mToken, @Path("sessionId") String sessionId);
}
