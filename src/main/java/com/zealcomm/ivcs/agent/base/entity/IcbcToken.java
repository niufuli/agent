package com.zealcomm.ivcs.agent.base.entity;

public class IcbcToken {


    /**
     * data : {"_id":"5ed1fff0f163b2000c208cb5","isDisabled":"N","org":"icbc-axa","userName":"fa_code_01","role":"agent","profile":{"userRole":"fa","fullName":"fa_name_01","marketingCode":"fa_code_01","branchCode":"branch_code","branchName":"branch_name","comCode":"com_code","comName":"comName"},"__v":0,"id":"5ed1fff0f163b2000c208cb5"}
     * iat : 1590820916
     * exp : 1590864116
     */

    private DataBean data;
    private int iat;
    private int exp;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getIat() {
        return iat;
    }

    public void setIat(int iat) {
        this.iat = iat;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public static class DataBean<T> {
        /**
         * _id : 5ed1fff0f163b2000c208cb5
         * isDisabled : N
         * org : icbc-axa
         * userName : fa_code_01
         * role : agent
         * profile : {"userRole":"fa","fullName":"fa_name_01","marketingCode":"fa_code_01","branchCode":"branch_code","branchName":"branch_name","comCode":"com_code","comName":"comName"}
         * __v : 0
         * id : 5ed1fff0f163b2000c208cb5
         */

        private String _id;
        private String isDisabled;
        private String org;
        private String userName;
        private String role;
        private T profile;
        private int __v;
        private String id;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getIsDisabled() {
            return isDisabled;
        }

        public void setIsDisabled(String isDisabled) {
            this.isDisabled = isDisabled;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public T getProfile() {
            return profile;
        }

        public void setProfile(T profile) {
            this.profile = profile;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
