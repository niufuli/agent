package com.zealcomm.ivcs.agent.base.entity;

public class RecordingListData {

    private String id;
    private Media media;
    private Storage storage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public static class Media {
        private Audio audio;
        private Video video;

        public Audio getAudio() {
            return audio;
        }

        public void setAudio(Audio audio) {
            this.audio = audio;
        }

        public Video getVideo() {
            return video;
        }

        public void setVideo(Video video) {
            this.video = video;
        }

        public static class Audio {
            private Format format;
            private String from;
            private String status;

            public Format getFormat() {
                return format;
            }

            public void setFormat(Format format) {
                this.format = format;
            }

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public static class Format {
                private Integer channelNum;
                private String codec;
                private int sampleRate;

                public Integer getChannelNum() {
                    return channelNum;
                }

                public void setChannelNum(Integer channelNum) {
                    this.channelNum = channelNum;
                }

                public String getCodec() {
                    return codec;
                }

                public void setCodec(String codec) {
                    this.codec = codec;
                }

                public int getSampleRate() {
                    return sampleRate;
                }

                public void setSampleRate(int sampleRate) {
                    this.sampleRate = sampleRate;
                }
            }
        }

        public static class Video {
            private Format format;
            private String from;
            private String status;

            public Format getFormat() {
                return format;
            }

            public void setFormat(Format format) {
                this.format = format;
            }

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public static class Format {
                private String codec;

                public String getCodec() {
                    return codec;
                }

                public void setCodec(String codec) {
                    this.codec = codec;
                }
            }
        }
    }

    public static class Storage {
        private String file;
        private String host;

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }
    }
}
