package com.zealcomm.ivcs.agent.base.entity;

import java.util.List;

public class OrgGroupData {


    /**
     * data : [{"_id":"5f40e05890dcce3389ee10d6","name":"查询","org":"zhy","createdBy":"5f2a4e8b58ae5b2dc8bda343","createdAt":"2020-08-22T09:07:36.453Z","formTemplates":[],"id":"5f40e05890dcce3389ee10d6"},{"_id":"5f40e03890dcce3389ee10d2","name":"贷款","org":"zhy","createdBy":"5f2a4e8b58ae5b2dc8bda343","createdAt":"2020-08-22T09:07:04.503Z","formTemplates":[{"_id":"5f40e05290dcce3389ee10d5","id":"5f40e04790dcce3389ee10d3"}],"id":"5f40e03890dcce3389ee10d2"}]
     * count : 2
     * page : 1
     * pageSize : 20
     */

    private int count;
    private int page;
    private int pageSize;
    private List<DataBean> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * _id : 5f40e05890dcce3389ee10d6
         * name : 查询
         * org : zhy
         * createdBy : 5f2a4e8b58ae5b2dc8bda343
         * createdAt : 2020-08-22T09:07:36.453Z
         * formTemplates : []
         * id : 5f40e05890dcce3389ee10d6
         */

        private String _id;
        private String name;
        private String org;
        private String createdBy;
        private String createdAt;
        private String id;
        private List<?> formTemplates;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<?> getFormTemplates() {
            return formTemplates;
        }

        public void setFormTemplates(List<?> formTemplates) {
            this.formTemplates = formTemplates;
        }
    }
}
