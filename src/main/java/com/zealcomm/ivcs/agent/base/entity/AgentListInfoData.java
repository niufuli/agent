package com.zealcomm.ivcs.agent.base.entity;

import java.util.List;

public class AgentListInfoData {

    private Integer count;
    private List<Data> data;
    private int page;
    private int pageSize;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public static class Data {
        private String agentId;
        private String agentName;
        private List<BelongGroups> belongGroups;
        private String currentStatus;
        private List<Integer> groups;
        private List<Invites> invites;
        private Session session;

        public String getAgentId() {
            return agentId;
        }

        public void setAgentId(String agentId) {
            this.agentId = agentId;
        }

        public String getAgentName() {
            return agentName;
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public List<BelongGroups> getBelongGroups() {
            return belongGroups;
        }

        public void setBelongGroups(List<BelongGroups> belongGroups) {
            this.belongGroups = belongGroups;
        }

        public String getCurrentStatus() {
            return currentStatus;
        }

        public void setCurrentStatus(String currentStatus) {
            this.currentStatus = currentStatus;
        }

        public List<Integer> getGroups() {
            return groups;
        }

        public void setGroups(List<Integer> groups) {
            this.groups = groups;
        }

        public List<Invites> getInvites() {
            return invites;
        }

        public void setInvites(List<Invites> invites) {
            this.invites = invites;
        }

        public Session getSession() {
            return session;
        }

        public void setSession(Session session) {
            this.session = session;
        }

        public static class Session {
            private String createdAt;
            private int id;
            private String media;
            private String org;
            private String roomId;
            private String sessionId;

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMedia() {
                return media;
            }

            public void setMedia(String media) {
                this.media = media;
            }

            public String getOrg() {
                return org;
            }

            public void setOrg(String org) {
                this.org = org;
            }

            public String getRoomId() {
                return roomId;
            }

            public void setRoomId(String roomId) {
                this.roomId = roomId;
            }

            public String getSessionId() {
                return sessionId;
            }

            public void setSessionId(String sessionId) {
                this.sessionId = sessionId;
            }
        }

        public static class BelongGroups {
            private int _id;
            private String createdAt;
            private int createdBy;
            private Group group;
            private int groupId;
            private int id;
            private int isChief;
            private int userId;
            private int weight;

            public int get_id() {
                return _id;
            }

            public void set_id(int _id) {
                this._id = _id;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public Group getGroup() {
                return group;
            }

            public void setGroup(Group group) {
                this.group = group;
            }

            public int getGroupId() {
                return groupId;
            }

            public void setGroupId(int groupId) {
                this.groupId = groupId;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getIsChief() {
                return isChief;
            }

            public void setIsChief(int isChief) {
                this.isChief = isChief;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getWeight() {
                return weight;
            }

            public void setWeight(int weight) {
                this.weight = weight;
            }

            public static class Group {
                private String createdAt;
                private int createdBy;
                private String defaultPolicy;
                private List<FormTemplates> formTemplates;
                private int id;
                private String name;
                private String org;
                private List<WorkflowSteps> workflowSteps;

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public String getDefaultPolicy() {
                    return defaultPolicy;
                }

                public void setDefaultPolicy(String defaultPolicy) {
                    this.defaultPolicy = defaultPolicy;
                }

                public List<FormTemplates> getFormTemplates() {
                    return formTemplates;
                }

                public void setFormTemplates(List<FormTemplates> formTemplates) {
                    this.formTemplates = formTemplates;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getOrg() {
                    return org;
                }

                public void setOrg(String org) {
                    this.org = org;
                }

                public List<WorkflowSteps> getWorkflowSteps() {
                    return workflowSteps;
                }

                public void setWorkflowSteps(List<WorkflowSteps> workflowSteps) {
                    this.workflowSteps = workflowSteps;
                }

                public static class FormTemplates {
                    private String createdAt;
                    private List<Fields> fields;
                    private String fromName;
                    private int id;

                    public String getCreatedAt() {
                        return createdAt;
                    }

                    public void setCreatedAt(String createdAt) {
                        this.createdAt = createdAt;
                    }

                    public List<Fields> getFields() {
                        return fields;
                    }

                    public void setFields(List<Fields> fields) {
                        this.fields = fields;
                    }

                    public String getFromName() {
                        return fromName;
                    }

                    public void setFromName(String fromName) {
                        this.fromName = fromName;
                    }

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public static class Fields {
                        private String name;

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }
                    }
                }

                public static class WorkflowSteps {
                    private int id;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }
                }
            }
        }

        public static class Invites {
            private String acceptedAt;
            private String code;
            private String createdAt;
            private From from;
            private String fromId;
            private String fromRole;
            private int id;
            private String localCcsUrl;
            private String privateCcsUrl;
            private String publicCcsUrl;
            private Session session;
            private String sessionId;
            private String status;
            private To to;
            private int toGroup;
            private String toId;
            private String toOrg;

            public String getAcceptedAt() {
                return acceptedAt;
            }

            public void setAcceptedAt(String acceptedAt) {
                this.acceptedAt = acceptedAt;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public From getFrom() {
                return from;
            }

            public void setFrom(From from) {
                this.from = from;
            }

            public String getFromId() {
                return fromId;
            }

            public void setFromId(String fromId) {
                this.fromId = fromId;
            }

            public String getFromRole() {
                return fromRole;
            }

            public void setFromRole(String fromRole) {
                this.fromRole = fromRole;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getLocalCcsUrl() {
                return localCcsUrl;
            }

            public void setLocalCcsUrl(String localCcsUrl) {
                this.localCcsUrl = localCcsUrl;
            }

            public String getPrivateCcsUrl() {
                return privateCcsUrl;
            }

            public void setPrivateCcsUrl(String privateCcsUrl) {
                this.privateCcsUrl = privateCcsUrl;
            }

            public String getPublicCcsUrl() {
                return publicCcsUrl;
            }

            public void setPublicCcsUrl(String publicCcsUrl) {
                this.publicCcsUrl = publicCcsUrl;
            }

            public Session getSession() {
                return session;
            }

            public void setSession(Session session) {
                this.session = session;
            }

            public String getSessionId() {
                return sessionId;
            }

            public void setSessionId(String sessionId) {
                this.sessionId = sessionId;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public To getTo() {
                return to;
            }

            public void setTo(To to) {
                this.to = to;
            }

            public int getToGroup() {
                return toGroup;
            }

            public void setToGroup(int toGroup) {
                this.toGroup = toGroup;
            }

            public String getToId() {
                return toId;
            }

            public void setToId(String toId) {
                this.toId = toId;
            }

            public String getToOrg() {
                return toOrg;
            }

            public void setToOrg(String toOrg) {
                this.toOrg = toOrg;
            }

            public static class From {
                private String id;
                private String role;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getRole() {
                    return role;
                }

                public void setRole(String role) {
                    this.role = role;
                }
            }

            public static class Session {
                private String id;
                private Url url;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public Url getUrl() {
                    return url;
                }

                public void setUrl(Url url) {
                    this.url = url;
                }

                public static class Url {
                    private String localUrl;
                    private String privateUrl;
                    private String publicUrl;

                    public String getLocalUrl() {
                        return localUrl;
                    }

                    public void setLocalUrl(String localUrl) {
                        this.localUrl = localUrl;
                    }

                    public String getPrivateUrl() {
                        return privateUrl;
                    }

                    public void setPrivateUrl(String privateUrl) {
                        this.privateUrl = privateUrl;
                    }

                    public String getPublicUrl() {
                        return publicUrl;
                    }

                    public void setPublicUrl(String publicUrl) {
                        this.publicUrl = publicUrl;
                    }
                }
            }

            public static class To {
                private int group;
                private String id;
                private String org;

                public int getGroup() {
                    return group;
                }

                public void setGroup(int group) {
                    this.group = group;
                }

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getOrg() {
                    return org;
                }

                public void setOrg(String org) {
                    this.org = org;
                }
            }
        }
    }
}
