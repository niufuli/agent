package com.zealcomm.ivcs.agent.base.entity;

import java.util.HashMap;
import java.util.List;

public class FormData {


    /**
     * code : 200
     * data : [{"_id":"5f210c61d0e1a2000d5831bf","sessionId":"21135651805051170","formTemplateId":"5f1c10f05cf963000826c2b8","fields":{"身高":"1","体重":"2"},"createdAt":"2020-07-29T05:42:57.662Z","updatedAt":"2020-07-29T05:42:57.662Z"}]
     */

    private int code;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * _id : 5f210c61d0e1a2000d5831bf
         * sessionId : 21135651805051170
         * formTemplateId : 5f1c10f05cf963000826c2b8
         * fields : {"身高":"1","体重":"2"}
         * createdAt : 2020-07-29T05:42:57.662Z
         * updatedAt : 2020-07-29T05:42:57.662Z
         */

        private String _id;
        private String sessionId;
        private String formTemplateId;
        private HashMap<String,String> fields;
        private String createdAt;
        private String updatedAt;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getFormTemplateId() {
            return formTemplateId;
        }

        public void setFormTemplateId(String formTemplateId) {
            this.formTemplateId = formTemplateId;
        }

        public HashMap<String,String> getFields() {
            return fields;
        }

        public void setFields(HashMap<String,String> fields) {
            this.fields = fields;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
