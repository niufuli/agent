package com.zealcomm.ivcs.agent.base.entity;

import java.util.List;

public class AmsLoginData {

    /**
     * services : ["serviceA","serviceB"]
     */
    private List<Integer> groups;

    public List<Integer> getGroups() {
        return groups;
    }

    public void setGroups(List<Integer> groups) {
        this.groups = groups;
    }
}
