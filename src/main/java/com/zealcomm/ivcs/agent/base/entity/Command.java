package com.zealcomm.ivcs.agent.base.entity;

public class Command {


    /**
     * cmd : take-photo
     * askForPermission : askForPermission || false
     */

    private String cmd;
    private String askForPermission;

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getAskForPermission() {
        return askForPermission;
    }

    public void setAskForPermission(String askForPermission) {
        this.askForPermission = askForPermission;
    }
}
