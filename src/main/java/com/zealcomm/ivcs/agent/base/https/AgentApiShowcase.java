package com.zealcomm.ivcs.agent.base.https;

import com.zealcomm.base.entity.LoginReq;
import com.zealcomm.base.entity.RecordBody;
import com.zealcomm.ivcs.agent.base.entity.FindAgents;
import com.zealcomm.ivcs.agent.base.entity.MonitorToken;
import com.zealcomm.ivcs.agent.base.entity.Recordings;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface AgentApiShowcase extends AgentApi {

    /**
     * 获取访客提交的用户信息
     */
    @GET("/ivcs/api/v1/sessions/{sessionId}/applications")
    Call<ResponseBody> getFormInfo(@Header("x-access-token") String token, @Path("sessionId") String sessionId);

    /**
     * 登录
     */
    @POST("/ivcs/api/v1/auth/logon/")
    Call<ResponseBody> login(@Body LoginReq tokenReq);
    /**
     * 提交图片
     */
    @Multipart
    @POST()
    Call<ResponseBody> uploadSignatureImg(@Url String url, @Part MultipartBody.Part part);

    @GET("/ivcs/api/v1/user/groups")
    Call<ResponseBody> getGroups(@Header("x-access-token") String token);

    @GET("/ivcs/api/v1/groups/{id}")
    Call<ResponseBody> getGroupsDetailWithName(@Header("x-access-token") String token, @Path("id") String id);

    @POST("/ivcs/rooms/{roomId}/recordings")
    Call<ResponseBody> startRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Body RecordBody recordBody);

    @DELETE("/ivcs/rooms/{roomId}/recordings/{recordId}")
    Call<ResponseBody> stopRecord(@Header("x-access-token") String token, @Path("roomId") String roomId, @Path("recordId") String recordId);

    @GET("/ivcs/api/v1/groups")
    Call<ResponseBody> getAllGroups(@Header("x-access-token") String token);

    @POST("/ivcs/api/v1/users/{userId}/compare-face")
    Call<ResponseBody> compareFace(@Header("x-access-token") String token, @Path("userId") String userId);

    @GET("/ivcs/api/v1/user-group-relations/isChief")
    Call<ResponseBody> isChief(@Header("x-access-token") String token, @Query("userId") String userId , @Query("groupId")String groupId);

    @POST("/ivcs/api/v1/agents/find")
    Call<ResponseBody> findAgents(@Header("x-access-token") String token, @Body FindAgents findAgents);

    @POST("/ivcs/api/v1/tokens")
    Call<ResponseBody> getMonitorToken(@Header("x-access-token") String token, @Body MonitorToken monitorToken);

    @POST("/ivcs/api/v1/rooms/{roomId}/recordings")
    Call<ResponseBody> recordings(@Header("x-access-token") String token, @Path("roomId") String roomId , @Body Recordings recordings);

    @GET("/ivcs/api/v1/sessions/{sessionId}/record")
    Call<ResponseBody> record(@Header("x-access-token") String mToken, @Path("sessionId") String sessionId);
}
