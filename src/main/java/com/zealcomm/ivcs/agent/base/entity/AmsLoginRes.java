package com.zealcomm.ivcs.agent.base.entity;

public class AmsLoginRes {


    /**
     * agentId : zhy
     * ticket : eyJhZ2VudElkIjoiNzg1MTEyMjQ1MDczMDMyODAwIiwidGlja2V0SWQiOiIweTYwcDhjMnRtMyIsIm5vdEJlZm9yZSI6MTU4NTAyOTI0MTc3Miwibm90QWZ0ZXIiOjE1ODUwMjk4NDE3NzIsInNpZ25hdHVyZSI6Ik5UTXhaREJtT0dVd1pEWXdPR1kwTUdNM09XVmpaakkyT1daa1l6STNOVEE1TVRRM05EazVNbVU1WlRBMU16VTJNMlExTlRNeU1ETmtZamd3TWpGaU5RPT0ifQ==
     * refersh_ticket_time : 60000
     */

    private String agentId;
    private String ticket;
    private int refersh_ticket_time;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getRefersh_ticket_time() {
        return refersh_ticket_time;
    }

    public void setRefersh_ticket_time(int refersh_ticket_time) {
        this.refersh_ticket_time = refersh_ticket_time;
    }
}
