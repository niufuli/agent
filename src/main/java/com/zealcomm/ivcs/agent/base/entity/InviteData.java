package com.zealcomm.ivcs.agent.base.entity;

public class InviteData {

    private String agentId;
    private String service;

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }
}
