package com.zealcomm.ivcs.agent.base.entity;

public class MonitorToken {

    private String room;
    private String user;
    private String role = "presenter";

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
