package com.zealcomm.ivcs.agent.base.entity;

public class RecordData {


    /**
     * id : 221602050843862980
     * media : {"audio":{"from":"5f3640843994590026a6e2fe-common","format":{"channelNum":2,"sampleRate":48000,"codec":"opus"},"status":"active"},"video":{"from":"5f3640843994590026a6e2fe-common","format":{"codec":"vp8"},"status":"active"}}
     * storage : {"host":"172.31.43.41","file":"/tmp/221602050843862980.mkv"}
     */

    private String id;
    private MediaBean media;
    private StorageBean storage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MediaBean getMedia() {
        return media;
    }

    public void setMedia(MediaBean media) {
        this.media = media;
    }

    public StorageBean getStorage() {
        return storage;
    }

    public void setStorage(StorageBean storage) {
        this.storage = storage;
    }

    public static class MediaBean {
        /**
         * audio : {"from":"5f3640843994590026a6e2fe-common","format":{"channelNum":2,"sampleRate":48000,"codec":"opus"},"status":"active"}
         * video : {"from":"5f3640843994590026a6e2fe-common","format":{"codec":"vp8"},"status":"active"}
         */

        private AudioBean audio;
        private VideoBean video;

        public AudioBean getAudio() {
            return audio;
        }

        public void setAudio(AudioBean audio) {
            this.audio = audio;
        }

        public VideoBean getVideo() {
            return video;
        }

        public void setVideo(VideoBean video) {
            this.video = video;
        }

        public static class AudioBean {
            /**
             * from : 5f3640843994590026a6e2fe-common
             * format : {"channelNum":2,"sampleRate":48000,"codec":"opus"}
             * status : active
             */

            private String from;
            private FormatBean format;
            private String status;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public FormatBean getFormat() {
                return format;
            }

            public void setFormat(FormatBean format) {
                this.format = format;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public static class FormatBean {
                /**
                 * channelNum : 2
                 * sampleRate : 48000
                 * codec : opus
                 */

                private int channelNum;
                private int sampleRate;
                private String codec;

                public int getChannelNum() {
                    return channelNum;
                }

                public void setChannelNum(int channelNum) {
                    this.channelNum = channelNum;
                }

                public int getSampleRate() {
                    return sampleRate;
                }

                public void setSampleRate(int sampleRate) {
                    this.sampleRate = sampleRate;
                }

                public String getCodec() {
                    return codec;
                }

                public void setCodec(String codec) {
                    this.codec = codec;
                }
            }
        }

        public static class VideoBean {
            /**
             * from : 5f3640843994590026a6e2fe-common
             * format : {"codec":"vp8"}
             * status : active
             */

            private String from;
            private FormatBeanX format;
            private String status;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public FormatBeanX getFormat() {
                return format;
            }

            public void setFormat(FormatBeanX format) {
                this.format = format;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public static class FormatBeanX {
                /**
                 * codec : vp8
                 */

                private String codec;

                public String getCodec() {
                    return codec;
                }

                public void setCodec(String codec) {
                    this.codec = codec;
                }
            }
        }
    }

    public static class StorageBean {
        /**
         * host : 172.31.43.41
         * file : /tmp/221602050843862980.mkv
         */

        private String host;
        private String file;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }
    }
}
