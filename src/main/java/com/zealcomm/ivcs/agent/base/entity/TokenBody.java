package com.zealcomm.ivcs.agent.base.entity;

public class TokenBody {
    private Data data;
    private int iat;
    private int exp;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getIat() {
        return iat;
    }

    public void setIat(int iat) {
        this.iat = iat;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public static class Data {
        private int id;
        private String userName;
        private String org;
        private String role;
        private String isDisabled;
        private boolean isRobot;
        private Profile profile;
        private String customerLevel;
        private int _id;
        private SchedulingAttributes schedulingAttributes;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getIsDisabled() {
            return isDisabled;
        }

        public void setIsDisabled(String isDisabled) {
            this.isDisabled = isDisabled;
        }

        public boolean getIsRobot() {
            return isRobot;
        }

        public void setIsRobot(boolean isRobot) {
            this.isRobot = isRobot;
        }

        public Profile getProfile() {
            return profile;
        }

        public void setProfile(Profile profile) {
            this.profile = profile;
        }

        public String getCustomerLevel() {
            return customerLevel;
        }

        public void setCustomerLevel(String customerLevel) {
            this.customerLevel = customerLevel;
        }

        public int get_id() {
            return _id;
        }

        public void set_id(int _id) {
            this._id = _id;
        }

        public SchedulingAttributes getSchedulingAttributes() {
            return schedulingAttributes;
        }

        public void setSchedulingAttributes(SchedulingAttributes schedulingAttributes) {
            this.schedulingAttributes = schedulingAttributes;
        }

        public static class Profile {
            private boolean isRobot;

            public boolean getIsRobot() {
                return isRobot;
            }

            public void setIsRobot(boolean isRobot) {
                this.isRobot = isRobot;
            }
        }

        public static class SchedulingAttributes {
            private String 是否机器人;

            public String get是否机器人() {
                return 是否机器人;
            }

            public void set是否机器人(String 是否机器人) {
                this.是否机器人 = 是否机器人;
            }
        }
    }
}
