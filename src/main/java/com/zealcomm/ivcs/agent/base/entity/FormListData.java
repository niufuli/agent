package com.zealcomm.ivcs.agent.base.entity;

import java.util.List;

public class FormListData {

    /**
     * _id : 5f1c11155cf963000826c2be
     * name : 查询
     * org : 1
     * createdBy : 5e6e0be1c30c11000e5f7b6f
     * createdAt : 2020-07-25T11:01:41.054Z
     * formTemplates : [{"_id":"5f1c10f05cf963000826c2b8","name":"身体调查","fields":[{"_id":"5f1c10f05cf963000826c2ba","name":"身高"},{"_id":"5f1c10f05cf963000826c2b9","name":"体重"}],"org":"1","createdBy":"5e6e0be1c30c11000e5f7b6f","createdAt":"2020-07-25T11:01:04.125Z","__v":0}]
     * __v : 0
     */

    private String _id;
    private String name;
    private String org;
    private String createdBy;
    private String createdAt;
    private int __v;
    private List<FormTemplatesBean> formTemplates;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public List<FormTemplatesBean> getFormTemplates() {
        return formTemplates;
    }

    public void setFormTemplates(List<FormTemplatesBean> formTemplates) {
        this.formTemplates = formTemplates;
    }

    public static class FormTemplatesBean {
        /**
         * _id : 5f1c10f05cf963000826c2b8
         * name : 身体调查
         * fields : [{"_id":"5f1c10f05cf963000826c2ba","name":"身高"},{"_id":"5f1c10f05cf963000826c2b9","name":"体重"}]
         * org : 1
         * createdBy : 5e6e0be1c30c11000e5f7b6f
         * createdAt : 2020-07-25T11:01:04.125Z
         * __v : 0
         */

        private String _id;
        private String name;
        private String org;
        private String createdBy;
        private String createdAt;
        private int __v;
        private List<FieldsBean> fields;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrg() {
            return org;
        }

        public void setOrg(String org) {
            this.org = org;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int get__v() {
            return __v;
        }

        public void set__v(int __v) {
            this.__v = __v;
        }

        public List<FieldsBean> getFields() {
            return fields;
        }

        public void setFields(List<FieldsBean> fields) {
            this.fields = fields;
        }

        public static class FieldsBean {
            /**
             * _id : 5f1c10f05cf963000826c2ba
             * name : 身高
             */

            private String _id;
            private String name;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }

}
