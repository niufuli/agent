package com.zealcomm.ivcs.agent.base.entity;

import com.google.gson.JsonObject;

public class InputForm {


    /**
     * inputform : 1
     * formTemplate:data,
     */

    private int inputform;

    public int getInputform() {
        return inputform;
    }

    public void setInputform(int inputform) {
        this.inputform = inputform;
    }

    private JsonObject formTemplate;

    public JsonObject getFormTemplate() {
        return formTemplate;
    }

    public void setFormTemplate(JsonObject formTemplate) {
        this.formTemplate = formTemplate;
    }
}
