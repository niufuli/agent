package com.zealcomm.ivcs.agent.base.entity;

import com.zealcomm.ccs.CcsMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HoldOnData {

    private String status = CcsMessage.CCS_HOLD_ON;

    private String at;

    public HoldOnData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        at = simpleDateFormat.format(new Date());
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAt() {
        return at;
    }

    public void setAt(String at) {
        this.at = at;
    }
}
