package com.zealcomm.ivcs.agent.base.entity;

public class AmsRingMsg {


    /**
     * session : {"id":"107958544783474990","url":"https://52.83.72.175:3091?addr=aHR0cHM6Ly8xMjcuMC4wLjE6MzAyMg==","group":"5f40e05890dcce3389ee10d6"}
     * userData : {"group":"5f40e05890dcce3389ee10d6"}
     */

    private SessionBean session;
    private UserDataBean userData;

    public SessionBean getSession() {
        return session;
    }

    public void setSession(SessionBean session) {
        this.session = session;
    }

    public UserDataBean getUserData() {
        return userData;
    }

    public void setUserData(UserDataBean userData) {
        this.userData = userData;
    }

    public static class SessionBean {
        /**
         * id : 107958544783474990
         * url : https://52.83.72.175:3091?addr=aHR0cHM6Ly8xMjcuMC4wLjE6MzAyMg==
         * group : 5f40e05890dcce3389ee10d6
         */

        private String id;
        private UrlBean url;
        private String group;
        private String room ;

        public String getRoom() {
            return room;
        }

        public void setRoom(String room) {
            this.room = room;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public static class UrlBean{
            private String localUrl ;
            private String privateUrl ;
            private String publicUrl ;

            public String getLocalUrl() {
                return localUrl;
            }

            public void setLocalUrl(String localUrl) {
                this.localUrl = localUrl;
            }

            public String getPrivateUrl() {
                return privateUrl;
            }

            public void setPrivateUrl(String privateUrl) {
                this.privateUrl = privateUrl;
            }

            public String getPublicUrl() {
                return publicUrl;
            }

            public void setPublicUrl(String publicUrl) {
                this.publicUrl = publicUrl;
            }
        }

        public UrlBean getUrl() {
            return url;
        }

        public void setUrl(UrlBean url) {
            this.url = url;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }
    }

    public static class UserDataBean {
        /**
         * group : 5f40e05890dcce3389ee10d6
         */
//"callMediaType": "video",
//        "receiveMediaType": "video",
//        "service": "5",
//        "specifyAgent": "",
//        "userId": "16"
        private String service;
        private String callMediaType ;
        private String receiveMediaType ;
        private String specifyAgent ;
        private String userId ;

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getCallMediaType() {
            return callMediaType;
        }

        public void setCallMediaType(String callMediaType) {
            this.callMediaType = callMediaType;
        }

        public String getReceiveMediaType() {
            return receiveMediaType;
        }

        public void setReceiveMediaType(String receiveMediaType) {
            this.receiveMediaType = receiveMediaType;
        }

        public String getSpecifyAgent() {
            return specifyAgent;
        }

        public void setSpecifyAgent(String specifyAgent) {
            this.specifyAgent = specifyAgent;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }


    }
}
