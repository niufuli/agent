package com.zealcomm.ivcs.agent.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.ValueCallback;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SignatureView extends WebView {
    public SignatureView(@NonNull Context context) {
        this(context , null);
    }

    public SignatureView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs , 0);
    }

    public SignatureView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getSettings().setAllowFileAccess(true);
        getSettings().setJavaScriptEnabled(true);
        loadUrl("file:///android_asset/signature/signature.html");
    }

    public void clearCanvas(){
        loadUrl("javascript:clearCanvas()");
    }

    public void loadFromJSON(String jsonValue){
        evaluateJavascript("javascript:loadFromJSON('" + jsonValue + "')", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                //此处为 js 返回的结果
                // Toast.makeText(H5Activity.this, value, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
