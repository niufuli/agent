package com.zealcomm.ivcs.agent;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.zealcomm.base.BaseManager;
import com.zealcomm.base.ServerCallback;
import com.zealcomm.base.entity.CustomMessage;
import com.zealcomm.base.entity.GroupData;
import com.zealcomm.base.entity.MediaOptions;
import com.zealcomm.ivcs.agent.base.AgentManager;
import com.zealcomm.ivcs.agent.base.entity.AgentListInfoData;
import com.zealcomm.ivcs.agent.base.entity.CollectedCustomerInfo;
import com.zealcomm.ivcs.agent.base.entity.CustomerInfoData;
import com.zealcomm.ivcs.agent.base.entity.FindAgents;
import com.zealcomm.ivcs.agent.base.entity.Recordings;

import org.webrtc.EglBase;
import org.webrtc.SurfaceViewRenderer;

import java.io.File;
import java.util.List;


public class IvcsAgent {

    private final String TAG = this.getClass().getName();
    private AgentManager mAgentManager;

    public IvcsAgent(Context context) {
        mAgentManager = new AgentManager(context);
    }

    /**
     * register ivcs agent event
     */
    public void registerIvcsEvent(IvcsAgentEvent ivcsAgentEvent) {
        mAgentManager.registerEvent(ivcsAgentEvent);
    }

    /**
     * init sdk
     * set url,user info etc.
     *
     * @param backendUrl            Account system services url
     * @param name                  user name or account
     * @param password              user password
     * @param org                   agency name
     * @param mediaOptions          configure the released video parameters
     * @param ivcsAgentInitCallback success or failed callback.
     */
    public void init(String backendUrl, String name, String password, String org, MediaOptions mediaOptions, IvcsAgentInitCallback ivcsAgentInitCallback) {
        mAgentManager.init(backendUrl, name, password, org, mediaOptions, new BaseManager.InitCallBack() {
            @Override
            public void initSuccess(String token, List<String> groupIds, List<GroupData> groupIdList) {
                ivcsAgentInitCallback.onSuccess(token, groupIds, groupIdList);
            }

            @Override
            public void initFailed(int errorCode, String error) {
                ivcsAgentInitCallback.onFailed(errorCode, error);
            }
        });
    }

    public void login(IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.login(ivcsAgentCallback);
    }

    public void compareFace() {
        mAgentManager.compareFace();
    }

    public void logout() {
        mAgentManager.logout();
    }

    public void obtainCustomerInfo(IvcsAgentCallback ivcsAgentCallback , CustomerInfoData data) {
        mAgentManager.obtainCustomerInfo(ivcsAgentCallback , data);
    }

    /**
     * checkIn
     *
     * @param groupsIds         use to register service type
     * @param ivcsAgentCallback success or failed callback.
     */
    public void checkIn(List<String> groupsIds, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.checkIn(groupsIds, ivcsAgentCallback);
    }

    /**
     * checkOut
     *
     * @param groupsIds         register service type
     * @param ivcsAgentCallback success or failed callback.
     */
    public void checkOut(List<String> groupsIds, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.checkOut(groupsIds, ivcsAgentCallback);
    }

    /**
     * ready receiving service requests
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void ready(IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.ready(ivcsAgentCallback);
    }

    /**
     * exit receiving service requests
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void unReady(IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.unReady(ivcsAgentCallback);
    }

    /**
     * Suspend receiving service requests
     *
     * @param reason            Reasons for suspension of service
     * @param ivcsAgentCallback success or failed callback.
     */
    public void otherWork(String reason, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.otherWork(reason, ivcsAgentCallback);
    }

    /**
     * In service, no service request will be received after the current service
     *
     * @param state             state for appoint
     * @param reason            Reasons for appoint
     * @param ivcsAgentCallback success or failed callback.
     */
    public void appoint(String state, String reason, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.appoint(state, reason, ivcsAgentCallback);
    }

    /**
     * Respond to service requests
     *
     * @param videoLabel        publish video label
     * @param ivcsAgentCallback success or failed callback.
     */
    public void answer(String videoLabel, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.answer(videoLabel, ivcsAgentCallback);
    }

    /**
     * Refuse to accept service request
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void reject(IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.reject(ivcsAgentCallback);
    }

    /**
     * End of service
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void hangup(IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.hangup(ivcsAgentCallback);
    }

    // TODO 待评审
    private void holdOn(IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.holdOn(ivcsAgentCallback);
    }

    /**
     * publish screen video
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void screenShare(@NonNull Intent data, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.shareScreen(data, ivcsAgentCallback);
    }

    /**
     * send guide frame request
     *
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendGuideReq(String userRole, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendPhotoGuide(userRole, ivcsAgentCallback);
    }

    /**
     * invite other user to join current room
     *
     * @param id                other service person id
     * @param groupId           other service person groupd id
     * @param ivcsAgentCallback success or failed callback.
     */
    public void inviteOther(String id, String groupId, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.inviteThrid(id, groupId, ivcsAgentCallback);
    }

    /**
     * Transfer to other service
     *
     * @param id                other service person id
     * @param groupId           other service person groupd id
     * @param ivcsAgentCallback success or failed callback.
     */
    public void transfer(String id, String groupId, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.transfer(id, groupId, new IvcsAgentCallback() {
            // 这个转接虽然名称是转接，但是执行的是邀请方法，在邀请成功后，要主动 hold-on
            @Override
            public void onSuccess() {
                holdOn(new IvcsAgentCallback() {

                    @Override
                    public void onSuccess() {
                        // hold-on 成功后，退出会话
                        mAgentManager.quitSession(new IvcsAgentCallback() {
                            @Override
                            public void onSuccess() {
                                ivcsAgentCallback.onSuccess();
                            }

                            @Override
                            public void onFailed(int errorCode, String error) {
                                ivcsAgentCallback.onFailed(errorCode, error);
                            }
                        });
                    }

                    @Override
                    public void onFailed(int errorCode, String error) {
                        ivcsAgentCallback.onFailed(errorCode, error);
                    }

                });
            }

            @Override
            public void onFailed(int errorCode, String error) {
                ivcsAgentCallback.onFailed(errorCode, error);
            }

        });
    }

    /**
     * switch front camera or rear camera
     */
    public void switchCamera() {
        mAgentManager.switchCamera();
    }

    /**
     * mute local publish stream video or audio
     *
     * @param audio audio if mute
     * @param video video if mute
     */
    public void muteLocal(boolean audio, boolean video) {
        if (audio) {
            mAgentManager.muteLocalStreamAudio(true);
        }
        if (video) {
            mAgentManager.muteLocalStreamVideo(true);
        }
    }

    /**
     * unmute local publish stream video or audio
     *
     * @param audio audio if unmute
     * @param video video if unmute
     */
    public void unmuteLocal(boolean audio, boolean video) {
        if (audio) {
            mAgentManager.muteLocalStreamAudio(false);
        }
        if (video) {
            mAgentManager.muteLocalStreamVideo(false);
        }
    }

    /**
     * attach video stream to render view
     *
     * @param videoLabel stream video label
     * @param renderView surface view
     */
    public void attachRenderWithLabel(String videoLabel, SurfaceViewRenderer renderView) {
        mAgentManager.registerRender(videoLabel, renderView);
    }

    /**
     * detach video stream to render view
     *
     * @param videoLabel stream video label
     * @param renderView surface view
     */
    public void detachRenderWithLabel(String videoLabel, SurfaceViewRenderer renderView) {
        mAgentManager.unregisterRender(videoLabel, renderView);
    }

    /**
     * send sign command to other user
     *
     * @param userRole          Command receiver
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendSignReq(String userRole, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendSign(userRole, ivcsAgentCallback);
    }

    /**
     * get form data
     */
    public void getFormData() {
        mAgentManager.getFormData();
    }

    /**
     * send form command to other user
     *
     * @param form              form data
     * @param userRole          Command receiver
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendFormReq(String form, String userRole, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendFormCmd(form, userRole, ivcsAgentCallback);
    }

    /**
     * send text message to other user
     *
     * @param text              text message
     * @param userRole          Command receiver
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendText(String text, String userRole, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendText(text, userRole, ivcsAgentCallback);
    }

    /**
     * stop screen share stream
     */
    public void stopScreenShare() {
        mAgentManager.stopShareScreen();
    }

    /**
     * send sanpshot command to other user
     *
     * @param userRole          Command receiver
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendScreenSanpshotReq(String userRole, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendSanpshot(userRole, ivcsAgentCallback);
    }

    /**
     * send link url to other user
     *
     * @param link              link url
     * @param userRole          Command receiver
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendLink(String link, String userRole, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendLink(link, userRole, ivcsAgentCallback);
    }

    /**
     * @param path              file path
     * @param type              pdf,txt,etc
     * @param ivcsAgentCallback success or failed callback.
     * @deprecated 由于 Android 权限问题，直接传路径不一定能获取到文件
     * send link url to other user
     */
    public void sendFile(String path, String userRole, String name, String type, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendFile(path, userRole, name, type, ivcsAgentCallback);
    }

    // TODO 待评审添加到文档
    public void sendFile(File file, String userRole, String name, String type, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendFile(file, userRole, name, type, ivcsAgentCallback);
    }

    /**
     * send custom message to other
     *
     * @param customMessage     message content
     * @param ivcsAgentCallback success or failed callback.
     */
    public void sendCustomMessage(CustomMessage customMessage, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.sendCoustomMessage(customMessage, ivcsAgentCallback);
    }

    public void getSubmitFormData() {
        mAgentManager.getSubmitFormData();
    }

    public EglBase.Context getEGLContext() {
        return mAgentManager.getEGLContext();
    }

    public void uninit() {
        mAgentManager.logout();
    }

    public List<String> getAllGroupsName() {
        return mAgentManager.getAllGroupNames();
    }

    public void isChief(String usserId, String groupId, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.isChief(usserId, groupId , ivcsAgentCallback);
    }

    public String getToken() {
        return mAgentManager.mToken;
    }

    public void findAgents(FindAgents findAgents, ServerCallback callback) {
        mAgentManager.findAgents(findAgents , callback) ;
    }

    public void getMonitorToken(String roomId , ServerCallback callback) {
        mAgentManager.getMonitorToken(roomId  , callback);
    }

    public void recordings(String roomId , Recordings recordings) {
        recordings(roomId, recordings, new ServerCallback() {
            @Override
            public void successResponse(String result) throws Exception {

            }

            @Override
            public void failedResponse(String result) {

            }
        });
    }

    private void recordings(String roomId , Recordings recordings, ServerCallback serverCallback) {
        mAgentManager.recordings(roomId , recordings , serverCallback) ;
    }

    public void record(String sessionId){
        record(sessionId, new ServerCallback() {
            @Override
            public void successResponse(String result) throws Exception {

            }

            @Override
            public void failedResponse(String result) {

            }
        });
    }

    private void record(String sessionId, ServerCallback serverCallback) {
        mAgentManager.record(sessionId , serverCallback) ;
    }

    public void joinRoom(String roomToken, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.joinRoom(roomToken , ivcsAgentCallback) ;
    }

    public void forceConnectCcs(String sessionId, String url, String query) {
        mAgentManager.forceConnectCcs(sessionId , url , query) ;
    }

    public void updateMonitorAgentInfo(AgentListInfoData.Data selectedAgent) {
        mAgentManager.updateMonitorAgentInfo(selectedAgent) ;
    }

    public void collectCustomerInfo(CollectedCustomerInfo collectedCustomerInfo, IvcsAgentCallback ivcsAgentCallback) {
        mAgentManager.collectCustomerInfo(collectedCustomerInfo , ivcsAgentCallback) ;
    }


    public interface IvcsAgentInitCallback {
        void onSuccess(String token, List<String> groupIds, List<GroupData> groupIdList);

        void onFailed(int errorCode, String error);
    }
}
